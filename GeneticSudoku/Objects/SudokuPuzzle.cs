﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace GeneticSudoku.Objects
{
    public class SudokuPuzzle
    {
        #region Attributes
        public readonly Square[,] Grid;
        public readonly List<Square>[] Rows;
        public readonly List<Square>[] Columns;
        public readonly List<Square>[] SubGrids;
        public readonly List<List<Square>> Groupings;
        public readonly List<uint> PossibleValues; 
        public readonly uint GridWidth;
        public readonly uint SubGridWidth;
        public readonly int LockedSquaresCount;
        public uint MaxValue { get { return GridWidth; } }
        public List<Square>[] GetRows 
        {
            get { return Rows; }
        }
        public Square this[int x, int y]
        {
            get { return Grid[x, y]; }
            protected set { Grid[x, y] = value; }
        } 
        #endregion // Attributes

        #region Puzzle Status Attributes and Statistics
        
        public bool UpdateStatus { get; protected set; }
        public bool UpdateSolved { get; protected set; }
        private int _blankSquaresCount;
        private bool _isSolved;
        
        public int BlankSquaresCount
        {
            get
            {
                if (UpdateStatus) _updateBlankSquares();
                return _blankSquaresCount;
            }
        }

        public bool IsSolved
        {
            get
            {
                if (UpdateSolved) _checkSolved();
                return _isSolved;
            }
        }

        /// <summary>
        /// Called by a square when it's value is changed to notify the puzzle that it's
        /// status and/or statistics (may) have changed since the last update
        /// </summary>
        /// <param name="sender">Square who raised the event</param>
        /// <param name="e">Relevant arguments</param>
        protected virtual void square_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            UpdateStatus = true;
            UpdateSolved = true;
        }

        /// <summary>
        /// Uses the rules of sudoku to evaluate the current board state and determine if
        /// the board's square values result in a 'solved' state
        /// </summary>
        private void _checkSolved()
        {
            if (!UpdateSolved) return;

            var noBlanks = BlankSquaresCount <= 0;

            var areBadGroups = Groupings.Count(g => PossibleValues.Except(g.Select(s => s.Value).ToList()).Any()) > 0;
            
            //!Groupings.Select(g => g.Select(s => s.Value).Distinct()).Any(g => g.Intersect(PossibleValues).Count() < MaxValue);

            _isSolved = noBlanks && !areBadGroups;
            UpdateSolved = false;
        }

        /// <summary>
        /// Updates the status and statistics gathering attributes for the class
        /// (only updates values when a Square.Value has been changed and notified
        /// the puzzle via a PropertyChanged event)
        /// </summary>
        private void _updateBlankSquares()
        {
            if (!UpdateStatus) return;

            // Count blank squares
            _blankSquaresCount = Grid.Cast<Square>().Count(sq => sq.IsBlank);

            // Reset update status
            UpdateStatus = false;
        }

        #endregion // Puzzle Status Attributes and Statistics

        #region Constructors

        public SudokuPuzzle(SudokuPuzzle p)
        {
            GridWidth = p.GridWidth;
            SubGridWidth = p.SubGridWidth;

            // Copy the Grid
            var g = new Square[GridWidth, GridWidth];
            for (var row = 0; row < GridWidth; row++)
                for (var col = 0; col < GridWidth; col++)
                    g[row, col] = new Square(MaxValue, p.Grid[row, col].Value, p.Grid[row, col].IsLocked);
            Grid = g;

            // Instantiate references lists
            var w = (int)GridWidth;
            Rows = new List<Square>[w];
            Columns = new List<Square>[w];
            SubGrids = new List<Square>[w];
            Groupings = new List<List<Square>>(w * 3);
            PossibleValues = new List<uint>(w);
            for (var i = 0; i < GridWidth; i++)
            {
                Rows[i] = new List<Square>(w);
                Columns[i] = new List<Square>(w);
                SubGrids[i] = new List<Square>(w);
                PossibleValues.Add((uint)i + 1);
            }

            LockedSquaresCount = p.LockedSquaresCount;
            UpdateSolved = p.UpdateSolved;
            UpdateStatus = p.UpdateStatus;

            AttachSudokuReferences();
        }

        public SudokuPuzzle(string serialized)
        {
            // Deserialize board
            Grid = Deserialize(serialized);
            GridWidth = (uint) Grid.GetLength(0);
            SubGridWidth = (uint) Math.Sqrt(GridWidth);

            // Instantiate references lists
            var w = (int) GridWidth;
            Rows = new List<Square>[w];
            Columns = new List<Square>[w];
            SubGrids = new List<Square>[w];
            Groupings = new List<List<Square>>(w*3);
            PossibleValues = new List<uint>(w);
            for (var i = 0; i < GridWidth; i++)
            {
                Rows[i] = new List<Square>(w);
                Columns[i] = new List<Square>(w);
                SubGrids[i] = new List<Square>(w);
                PossibleValues.Add((uint) i + 1);
            }
            
            // Clear statistics
            LockedSquaresCount = Grid.Cast<Square>().Count(sq => sq.IsLocked);
            _blankSquaresCount = 0;

            // Set updates true = Must update statistics on first run (status data doesn't exist until observed!)
            UpdateStatus = true;
            UpdateSolved = true;

            // Set sudoku references
            AttachSudokuReferences();
        }

        public SudokuPuzzle(uint[,] givens)
        {
            if (givens.GetLength(0) != givens.GetLength(1))
                throw new ArgumentOutOfRangeException("givens");

            GridWidth = (uint) givens.GetLength(0);
            SubGridWidth = (uint)Math.Sqrt(GridWidth);
            if(givens.Cast<uint>().Any(v => v > GridWidth))
                throw new InvalidDataException("Given value is out of sudoku range");

            var w = (int) GridWidth;
            Grid = BuildGridGromGivensArray(givens);
            Rows = new List<Square>[w];
            Columns = new List<Square>[w];
            SubGrids = new List<Square>[w];
            PossibleValues = new List<uint>(w);
            Groupings = new List<List<Square>>(w * 3);

            for (var i = 0; i < w; i++)
            {
                Rows[i] = new List<Square>(w);
                Columns[i] = new List<Square>(w);
                SubGrids[i] = new List<Square>(w);
                PossibleValues.Add((uint) i + 1);
            }

            // Clear statistics
            LockedSquaresCount = Grid.Cast<Square>().Count(sq => sq.IsLocked);
            _blankSquaresCount = 0;

            // Set updates true = Must update statistics on first run (status data doesn't exist until observed!)
            UpdateStatus = true;
            UpdateSolved = true;

            AttachSudokuReferences();
        }
        #endregion // Constructors

        #region Helpers
        /// <summary>
        /// Determines if given number of total squares is a valid
        /// amount of squares for a sudoku board
        /// </summary>
        /// <param name="n">Number of squares</param>
        /// <returns>True if n is a valid total square count; false otherwise</returns>
        private static bool IsValidSquareCount(uint n)
        {
            return n >= 16 && IsPerfectSquare(n);
        }

        /// <summary>
        /// Determines if given unsigned int is a perfect square
        /// </summary>
        /// <param name="n">Number to test</param>
        /// <returns>True if perfect square; false otherwise</returns>
        private static bool IsPerfectSquare(uint n)
        {
            var sqrt = (uint)Math.Sqrt(n);
            return n == sqrt * sqrt;
        }
        #endregion //Helpers

        #region Serialize/Deserialize
        /// <summary>
        /// Serializes the board's values to the proper comma-delimited format that
        /// can be deserialized when passed to a SudokuPuzzle constructor
        /// </summary>
        /// <returns>CSV-style string representation of the sudoku grid</returns>
        public static string Serialize(SudokuPuzzle sudoku)
        {
            var s = new StringBuilder();

            var g = sudoku.Grid.Cast<Square>().ToList();
            for (var i = 0; i < g.Count; i++)
            {
                var sq = g[i];
                s.Append(sq);
                if (sq.IsLocked) s.Append("*");
                if (i < g.Count - 1) s.Append(",");
            }

            return s.ToString();
        }

        /// <summary>
        /// Parses a properly-formatted (CSV-style) string representing a sudoku
        /// grid and returns a corresponding sudoku grid (as a 2d-array
        /// of Square objects)
        /// </summary>
        /// <param name="serialized">Properly-formatted string representing a sudoku grid</param>
        /// <returns>Corresponding sudoku grid (as a new 2d-array of Square objects)</returns>
        private static Square[,] Deserialize(string serialized)
        {
            // Validate input string and its format
            const string header = "Serialized Sudoku Board";
            if (serialized == null)
                throw new NoNullAllowedException(header);

            var data = serialized.Split(',');
            var valCount = (uint)data.Count();

            if (!IsValidSquareCount(valCount))
                throw new FormatException(header + ": " + data.Count() + " is not a valid number of squares");

            // Parse the individual square datum
            var width = (uint)Math.Sqrt(valCount);
            var squares = new Queue<Square>((int)valCount);
            foreach (var capt in data.Select(d => Regex.Match(d, @"^[\n\s]*(?:(?<val>(?:\d+))(?<req>\*)?|(?<val>\.))[\n\s]*$")))
            {
                if (!capt.Success)
                    throw new FormatException(header + ": invalid square value present");

                var str = capt.Groups["val"].Value;
                var req = capt.Groups["req"].Value;
                var value = str == "." ? 0 : uint.Parse(str);
                var isReq = req == "*";

                squares.Enqueue(new Square(width, value, isReq));
            }

            // Build the board
            var puzzle = new Square[width, width];

            // Create the squares and fill in the givens
            for (var row = 0; row < width; row++)
                for (var col = 0; col < width; col++)
                    puzzle[row, col] = squares.Dequeue();
            
            return puzzle;
        }
        #endregion // Serialize/Deserialize

        #region Private Methods

        /// <summary>
        /// Sets row, column, and sub square references for board's squares
        /// (Also attaches PropertyChanged delegates to squares)
        /// </summary>
        private void AttachSudokuReferences()
        {
            var baseGroup = 0;
            for (var row = 0; row < GridWidth; row++)
            {
                var offset = 0;
                for (var col = 0; col < GridWidth; col++)
                {
                    var sub = baseGroup + offset;
                    Rows[row].Add(this[row, col]);
                    Columns[col].Add(this[row, col]);
                    SubGrids[sub].Add(this[row, col]);
                    this[row, col].PropertyChanged += square_PropertyChanged;
                    if ((col + 1) % SubGridWidth == 0) offset++;
                }
                if ((row + 1)%SubGridWidth == 0) baseGroup += (int)SubGridWidth;
            } 

            Groupings.AddRange(Rows);
            Groupings.AddRange(Columns);
            Groupings.AddRange(SubGrids);
        }

        /// <summary>
        /// Used by the constructor to create the squares for the board
        /// and fill in their default values (givens and blanks)
        /// </summary>
        /// <param name="values">2D array of values</param>
        private Square[,] BuildGridGromGivensArray(uint[,] values)
        {
            // Create the squares and fill in their values (locking the givens)
            var g = new Square[GridWidth, GridWidth];
            for (var row = 0; row < GridWidth; row++)
                for (var col = 0; col < GridWidth; col++)
                    g[row, col] = new Square(GridWidth, values[row, col], values[row, col] != 0);
            return g;
        }

        /// <summary>
        /// Sets all non-given board values to 0 (blank)
        /// </summary>
        public void ClearNonGivens()
        {
            lock (Grid)
            {
                foreach (var s in Grid.Cast<Square>().Where(s => !s.IsLocked).ToList())
                    s.Value = 0;
            }
        }

        #endregion // Private Methods

        #region Public Methods

        /// <summary>
        /// Dumps a text-based visualization of the sudoku board along with some related statistics
        /// </summary>
        public virtual void Dump()
        {
            Console.WriteLine("\nSudoku Board State:");
            var spaces = new StringBuilder("  ");
            var dashes = new StringBuilder("--");
            var n = MaxValue;
            var digitSpace = 2;
            while (n > 1)
            {
                spaces.Append(" ");
                dashes.Append("-");
                n /= 10;
                digitSpace++;
            }
            var spacer = spaces.ToString();
            var blankVal = dashes.ToString();
            var fDigit = "{0," + digitSpace + "}";

            // Generate the proper size divider line
            var d = new StringBuilder(" ");
            if (SubGridWidth > 1)
            {
                for (var sp = 0; sp <= GridWidth; sp++)
                    d.Append(blankVal);
            }
            for (var sp = 1; sp < SubGridWidth; sp++)
                d.Append("--");

            var divider = d.ToString();
            const string wall = " |";

            // Fill in values
            for (var i = 0; i < GridWidth; i++)
            {
                if (i % SubGridWidth == 0)
                    Console.WriteLine(divider);

                for (var j = 0; j < GridWidth; j++)
                {
                    if (j % SubGridWidth == 0)
                        Console.Write(wall);

                    Console.ForegroundColor = this[i, j].IsLocked ? ConsoleColor.Blue : ConsoleColor.Yellow;

                    if (this[i, j].Value == 0)
                        Console.Write(spacer);
                    else
                        Console.Write(fDigit, this[i, j]); // Implied string.Format()
                        
                    Console.ResetColor();
                }
                Console.WriteLine(wall);
            }

            Console.WriteLine(divider);

            Console.WriteLine("      Size: " + GridWidth + "x" + GridWidth + " (" + GridWidth * GridWidth + " squares, " + LockedSquaresCount + " given)");
            Console.WriteLine("     Blank: " + BlankSquaresCount);
            Console.WriteLine("    Solved: " + (IsSolved ? "YES" : "NO"));
        }

        #endregion // Public Methods
    }
}
