﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace GeneticSudoku.Objects
{
    public class GASudokuPuzzle : SudokuPuzzle
    {
        #region Attributes
        private readonly Random R;
        private int _fitnessScore;
        public bool UpdateFitness;
        #endregion // Attributes

        #region Puzzle Status Attributes and Statistics

        private void _checkFitness()
        {
            if (!UpdateFitness) return;
            _fitnessScore = TotalFitness();
            UpdateFitness = false;
        }

        public int Fitness
        {
            get
            {
                if (UpdateFitness) _checkFitness();
                return _fitnessScore;
            }
        }

        /// <summary>
        /// Called by a square when it's value is changed to notify the puzzle that it's
        /// status and/or statistics (may) have changed since the last update
        /// </summary>
        /// <param name="sender">Square who raised the event</param>
        /// <param name="e">Relevant arguments</param>
        protected override void square_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            UpdateStatus = true;
            UpdateSolved = true;
            UpdateFitness = true;
        }

        #endregion // Puzzle Status Attributes and Statistics

        #region Static Methods
        
        #endregion Static Methods

        #region Constructors
        public GASudokuPuzzle(SudokuGA ga, string serialized) : base(serialized)
        {
            R = ga.GARandom;
            UpdateFitness = true;
        }

        public GASudokuPuzzle(GASudokuPuzzle p) : base(p)
        {
            R = p.R;
            UpdateFitness = true;
        }
        #endregion // Constructors

        #region LINQ Fitness

        /// <summary>
        /// Calculates the number of squares that are out of place in all rows/cols/sub-grids
        /// for the given grouping
        /// </summary>
        /// <param name="g">Rows, Columns, or Subgrids grouping to determine fitness for</param>
        /// <returns>The groupings fitness score</returns>
        private int OutOfPlaceLINQ(IEnumerable<List<Square>> g)
        {
            // Count the sqaures that are out of place (don't count the locked squares)
            var sqRowFitness = (from rlist in g
                                let lockedSquaresValues = rlist.Where(s => s.IsLocked).Select(s => s.Value)
                                let rvals = rlist.Select(s => s.Value).ToList()
                                let p = PossibleValues.Except(lockedSquaresValues).ToList()
                                select p.Except(rvals, new UintComparer()).ToList()
                                    into r
                                    select r.Count).ToList();
            var fitness = sqRowFitness.Sum();
            return fitness;
        }

        /// <summary>
        /// Counts the number of rows/columns/sub-grids the grouping contains that do not
        /// contain only valid sudoku number sequences 
        /// </summary>
        /// <param name="g">Grouping (rows/columns/sub-grids) to test</param>
        /// <returns># of invalid (rows/columns/sub-grids) within the grouping</returns>
        private int GroupingFitnessLINQ(IEnumerable<List<Square>> g)
        {
            var rfitness = (from rlist in g
                            select rlist.Select(s => s.Value).ToList()
                                into rvals
                                select PossibleValues.Except(rvals, new UintComparer()).ToList()
                                    into r
                                    select r.Count);
            return rfitness.Count(i => i > 0);
        }

        /// <summary>
        /// Determines fitness of current state of sudoku puzzle (LINQ-Based)
        /// Test runs have shown that this can run up to TWICE AS SLOW as the
        /// 'normal' version. Both return the same value.
        /// </summary>
        /// <returns></returns>
        public int TotalFitnessLINQ()
        {
            return GroupingFitnessLINQ(Rows) + GroupingFitnessLINQ(Columns);
        }

        #endregion // LINQ Fitness

        #region Regular Fitness

        /// <summary>
        /// Calculates sum of individual square fitnesses for a grouping with loops and manual
        /// counting.
        /// Less-readable code, but nearly twice as fast.
        /// </summary>
        /// <param name="g">Rows, Columns, or Subgrids grouping to determine fitness for</param>
        /// <returns>The groupings fitness score</returns>
        private static int TotalGroupFitness(IEnumerable<List<Square>> g)
        {
            var squaresAlreadyChecked = new List<Square>();
            var rowFitness = 0;
            foreach (var rlist in g)
            {
                // Flags to determine which values have been seen already
                var existingValues = new List<uint>(rlist.Count);

                //
                // Count empty squares as bad, but don't double count
                //
                foreach (var sq in rlist.Where(s => s.IsBlank && !squaresAlreadyChecked.Contains(s)))
                {
                    squaresAlreadyChecked.Add(sq);
                    rowFitness++;
                }

                //
                // Locked values are always in the right spot. Flag any duplicates as bad
                //
                foreach (var sq in rlist.Where(s => s.IsLocked))
                {
                    squaresAlreadyChecked.Add(sq);
                    existingValues.Add(sq.Value);

                    // Count duplicates of a locked value as invalid squares
                    var sq1 = sq;
                    var dupes = rlist.Where(s => (s != sq1 && s.Value == sq1.Value));
                    foreach (var dupe in dupes.Where(dupe => !squaresAlreadyChecked.Contains(dupe)))
                    {
                        squaresAlreadyChecked.Add(dupe);
                        rowFitness++;
                    }
                }

                //
                // Only things left are squares that are user-filled-in. All blanks and locks are accounted for.
                // All user-filled-in values that conflict with a locked (given) value are also accounted for.
                //
                foreach (var sq in rlist.Where(s => !(s.IsLocked && s.IsBlank && squaresAlreadyChecked.Contains(s) && existingValues.Contains(s.Value))))
                {
                    squaresAlreadyChecked.Add(sq);
                    existingValues.Add(sq.Value);

                    // Count duplicates of a unique value as invalid squares
                    var sq1 = sq;
                    var dupes = rlist.Where(s => (s != sq1) && (s.Value == sq1.Value) && !squaresAlreadyChecked.Contains(s));
                    foreach (var dupe in dupes)
                    {
                        squaresAlreadyChecked.Add(dupe);
                        rowFitness++;
                    }
                }
            }
            return rowFitness;
        }

        /// <summary>
        /// Determines fitness of current state of sudoku puzzle.
        /// Tests have shown this is consistently faster than the LINQ method
        /// </summary>
        /// <returns></returns>
        public int TotalFitness()
        {
            return TotalGroupFitness(Rows) + TotalGroupFitness(Columns) + TotalGroupFitness(SubGrids);
        }

        #endregion // Regular Fitness

        #region Public Methods

        public static void Mutate(GASudokuPuzzle p, Random r)
        {
            // Select 2 unique squares from grid whose values aren't locked
            var mutable = p.Grid.Cast<Square>().Where(s => !s.IsLocked).ToList();
            var s1 = mutable[r.Next(mutable.Count)];
            mutable.Remove(s1);
            var s2 = mutable[r.Next(mutable.Count)];

            // Swap their values
            var tmp = s1.Value;
            s1.Value = s2.Value;
            s2.Value = tmp;
        }

        /// <summary>
        /// Randomly selects a sub-grid and swaps the positions of two
        /// of the sub-grid's non-locked values. If the new fitness is
        /// better than the original, the mutation is kept. If not, the
        /// mutation is reverted by resetting the squares' values back to
        /// their pre-mutation values
        /// </summary>
        public void MutateWithinSubGrid()
        {
            //var oldFitness = Fitness;
            var grid = SubGrids[R.Next(SubGrids.Count())];
            var ableToMutate = grid.Where(s => !s.IsLocked).ToList();
            
            // Minimum # of non-locked required is 2
            if (ableToMutate.Count < 2) return;

            var s1 = ableToMutate[R.Next(ableToMutate.Count())];
            ableToMutate.Remove(s1);
            var s2 = ableToMutate[R.Next(ableToMutate.Count())];

            var tmp = s1.Value;
            s1.Value = s2.Value;
            s2.Value = tmp;
        }

        /// <summary>
        /// Fills the blank squares with random values
        /// </summary>
        public void FillBlanksDumbRandomly()
        {
            lock (Grid)
            {
                foreach (var sq in Grid.Cast<Square>().Where(s => s.IsBlank))
                {
                    sq.Value = PossibleValues[R.Next(PossibleValues.Count)];
                }
            }
        }

        /// <summary>
        /// Fills the blank squares with random values 
        /// (Enures all sub-grids are valid, but not rows & columns)
        /// </summary>
        public void FillBlanksSmartRandomly()
        {
            lock (Grid)
            {
                foreach (var sq in Grid.Cast<Square>().Where(s => s.IsBlank))
                {
                    var sub = SubGrids.First(l => l.Contains(sq));
                    var currentSubVals = sub.Where(s => !s.IsBlank).Select(s => s.Value).ToList();
                    var possibleSubVals = PossibleValues.Except(currentSubVals).ToList();
                    sq.Value = possibleSubVals[R.Next(possibleSubVals.Count)];
                }
            }
        }

        /// <summary>
        /// Dumps a text-based visualization of the sudoku board, related statistics, and its fitness
        /// </summary>
        public override void Dump()
        {
            base.Dump();
            Console.WriteLine("   Fitness: " + Fitness);
        }

        #endregion // Public Methods
    }
}
