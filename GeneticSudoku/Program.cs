﻿using System;
using System.Linq;
using System.Threading.Tasks;
using GeneticSudoku.Objects;

namespace GeneticSudoku
{
    class Program
    {
        // ReSharper disable UnusedMember.Local
        private const string absurdlyEasy4x4 = "3*,.,4*,.," + ".,1*,.,2*," + ".,4*,.,3*," + "2*,.,1*,.";

        private const string merelyEasy4x4 = ".,1*,3*,.," +
                                             ".,.,.,4*," +
                                             ".,.,.,1*," +
                                             ".,2*,4*,.";

        private const string almostTough4x4 = ".,2*,.,.," +
                                              "1*,.,2*,.," +
                                              "2*,.,3*,.," +
                                              ".,3*,.,.";

        private const string p4x4 = "1,2*,3,4," +
                                    "1,2,3,4," +
                                    "1,2,3,4," +
                                    "1,2,3,4";

        private const string p4x4_blank = ".,.,.,.," +
                                          ".,.,.,.," +
                                          ".,.,.,.," +
                                          ".,.,.,.";

        private const string p16x16 = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16";
        private const string _easy9x9 = ".,.,6*,.,2*,.,5*,4*,.," +
                                        "4*,2*,.,.,.,9*,.,8*,.," +
                                        "8*,.,7*,.,3*,.,9*,.,.," +
                                        ".,4*,1*,.,.,5*,2*,6*,.," +
                                        "7*,.,.,.,.,.,.,.,1*," +
                                        ".,6*,2*,3*,.,.,8*,7*,.," +
                                        ".,.,5*,.,7*,.,4*,.,8*," +
                                        ".,9*,.,2*,.,.,.,3*,7*," +
                                        ".,7*,8*,.,4*,.,1*,.,.";

        private const string p9x9_blank = ".,.,.,.,.,.,.,.,.," +
                                          ".,.,.,.,.,.,.,.,.," +
                                          ".,.,.,.,.,.,.,.,.," +
                                          ".,.,.,.,.,.,.,.,.," +
                                          ".,.,.,.,.,.,.,.,.," +
                                          ".,.,.,.,.,.,.,.,.," +
                                          ".,.,.,.,.,.,.,.,.," +
                                          ".,.,.,.,.,.,.,.,.," +
                                          ".,.,.,.,.,.,.,.,.";

        private const string p9x9_Solution = "9,1,6*,8,2*,7,5*,4*,3," +
                                          "4*,2*,3,5,1,9*,7,8*,6," +
                                          "8*,5,7*,6,3*,4,9*,1,2," +
                                          "3,4*,1*,7,8,5*,2*,6*,9," +
                                          "7*,8,9,4,6,2,3,5,1*," +
                                          "5,6*,2*,3*,9,1,8*,7*,4," +
                                          "2,3,5*,1,7*,6,4*,9,8*," +
                                          "1,9*,4,2*,5,8,6,3*,7*," +
                                          "6,7*,8*,9,4*,3,1*,2,5";

        private const string medium9x9 = ".,.,1*,.,7*,.,.,6*,8*,4*,5*,.,.,.,1*,9*,3*,.,6*,.,.,.,.,3*,1*,.,.,5*,1*,.,.,.,.,.,.,.,.,.,6*,.,.,.,4*,.,.,.,.,.,.,.,.,.,7*,3*,.,.,5*,3*,.,.,.,.,4*,.,2*,7*,6*,.,.,.,1*,9*,9*,3*,.,.,1*,.,8*,.,.";
        private const string hard_9x9 = ".,.,.,.,3*,.,.,6*,.,5*,.,.,.,4*,6*,8*,2*,7*,.,.,8*,.,.,.,.,.,.,.,.,.,5*,.,8*,.,.,6*,.,3*,.,.,9*,.,.,5*,.,4*,.,.,3*,.,1*,.,.,.,.,.,.,.,.,.,3*,.,.,3*,1*,6*,9*,8*,.,.,.,4*,.,2*,.,.,1*,.,.,.,.";
        // ReSharper restore UnusedMember.Local

        private const double MRate = 0.35;
        private const uint PopSize = 2500;
        private const uint Gens = 0;
        private const uint TSize = 2;

        public static void FireMissiles()
        {
            var crowdcount = 10;
            var crowd = new SudokuGA[crowdcount];
            var a = new Task[crowdcount];
            for (var i = 0; i < crowdcount; i++)
            {
                crowd[i] = new SudokuGA(hard_9x9, MRate, PopSize, 0, tourneySize: TSize);
                var i1 = i;
                a[i] = Task.Factory.StartNew(() => crowd[i1].GeneratePopulation());
            }
            Task.WaitAll(a);

            var missiles = new Task[crowdcount];
            for (var i = 0; i < crowdcount; i++)
            {
                missiles[i] = Task.Factory.StartNew(() => crowd[i].Run());
            }
            Task.WaitAny(missiles);
        }

        public static void WoC()
        {
            var crowdcount = 10;
            var crowd = new SudokuGA[crowdcount];
            for (var i = 0; i < crowdcount; i++)
            {
                crowd[i] = new SudokuGA(medium9x9, MRate, PopSize, 10000, tourneySize: TSize);
            }

            
            var a = new Task[crowdcount];
            for (var i = 0; i < crowdcount; i++)
            {
                var i1 = i;
                a[i] = Task.Factory.StartNew(() => crowd[i1].GeneratePopulation());
            }
            Task.WaitAll(a);

            var b = new Task[crowdcount];
            for (var i = 0; i < crowdcount; i++)
            {
                var i1 = i;
                b[i] = Task.Factory.StartNew(() => crowd[i1].Run());
            }
            Task.WaitAll(b);

            Console.ReadLine();
        }

        public static void TestGenerations()
        {
            var ga = new SudokuGA(medium9x9, MRate, PopSize, Gens, tourneySize: TSize);
            
            ga.GeneratePopulation(false);
            ga.Population.First().Dump();
            Console.ReadLine();

            var i = 0;
            while (true)
            {
                ga.NextGeneration();
                var psorted = ga.Population.Select(p => p.Fitness).Distinct().OrderBy(v => v).ToList();

                if (i >= 10)
                {
                    Console.WriteLine("Unique Fitnesses: {0}, Fittest: {1}", psorted.Count, psorted.First());
                    i = 0;
                }
                else
                {
                    i++;
                }

                if (ga.Population.Any(p => p.IsSolved)) break;
            }

            var sol = ga.Population.Where(p => p.IsSolved).ToList().First();
            sol.Dump();
            var correct = SudokuPuzzle.Serialize(sol) == p9x9_Solution;
            //Console.WriteLine("Answer correct?: " + (correct ? "YES" : "NO"));
            Console.WriteLine("Time: {0}\nGens: {1}", (ga.W.ElapsedMilliseconds/1000.000), ga.CurrentGeneration);
            Console.ReadLine();

        }

        static void Main()
        {
            FireMissiles();
            //TestGenerations();
        }
        
        public static void TestSudokuPuzzleActions()
        {
            //var p1 = new SudokuPuzzle(p4x4_blank);
            //var p1 = new SudokuPuzzle(p16x16);
            //var p1 = new SudokuPuzzle(_easy9x9);

            var sol = new SudokuPuzzle(p9x9_Solution);
            sol.Dump();
            Console.ReadLine();

            sol.ClearNonGivens();
            sol.Dump();
            Console.ReadLine();

            
        }

        public static void SingleGAPuzzle_FillBlanksRandomly()
        {
            var ga = new SudokuGA(_easy9x9, 0.02, 10, 10, tourneySize: 2);
            var g1 = new GASudokuPuzzle(ga, _easy9x9);
            g1.Dump();
            Console.ReadLine();

            g1.FillBlanksSmartRandomly();
            g1.Dump();
            Console.ReadLine();
        }

        public static void GenerateAPopulation()
        {
            var ga = new SudokuGA(_easy9x9, 0.02, 10, 10);
            ga.GeneratePopulation();
            foreach (var s in ga.Population)
            {
                Console.WriteLine(s.Fitness);
            }
            Console.ReadLine();
        }
    }
}
