﻿using System;
using System.Collections.Generic;
using System.Linq;
using GeneticSudoku.Objects;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GeneticSudokuTests.Objects
{
    [TestClass]
    public class SudokuPuzzleTests
    {
        #region Test Properties

        // Easy puzzle: http://www.websudoku.com/?level=1&set_id=3900060771
        private const string _easy9x9 = ".,.,6*,.,2*,.,5*,4*,.," +
                                        "4*,2*,.,.,.,9*,.,8*,.," +
                                        "8*,.,7*,.,3*,.,9*,.,.," +
                                        ".,4*,1*,.,.,5*,2*,6*,.," +
                                        "7*,.,.,.,.,.,.,.,1*," +
                                        ".,6*,2*,3*,.,.,8*,7*,.," +
                                        ".,.,5*,.,7*,.,4*,.,8*," +
                                        ".,9*,.,2*,.,.,.,3*,7*," +
                                        ".,7*,8*,.,4*,.,1*,.,.";

        private static readonly uint[,] _easy9x9Array = new uint[,]
            {
                {0,0,6,0,2,0,5,4,0},
                {4,2,0,0,0,9,0,8,0},
                {8,0,7,0,3,0,9,0,0},
                {0,4,1,0,0,5,2,6,0},
                {7,0,0,0,0,0,0,0,1},
                {0,6,2,3,0,0,8,7,0},
                {0,0,5,0,7,0,4,0,8},
                {0,9,0,2,0,0,0,3,7},
                {0,7,8,0,4,0,1,0,0}
            };

        private const string _easy9x9Solution = "9,1,6*,8,2*,7,5*,4*,3," +
                                                "4*,2*,3,5,1,9*,7,8*,6," +
                                                "8*,5,7*,6,3*,4,9*,1,2," +
                                                "3,4*,1*,7,8,5*,2*,6*,9," +
                                                "7*,8,9,4,6,2,3,5,1*," +
                                                "5,6*,2*,3*,9,1,8*,7*,4," +
                                                "2,3,5*,1,7*,6,4*,9,8*," +
                                                "1,9*,4,2*,5,8,6,3*,7*," +
                                                "6,7*,8*,9,4*,3,1*,2,5";


        private const string _invalidRequiredBlank = "1,2*,3,4,.,2,.*,4,1,2*,3,4,.,2,.*,4";
        
        private const uint _width = 9;
        private const uint _subWidth = 3;
        private const uint _squareCount = 81;
        private const int _numGivens = 36;
        #endregion // Test Properties

        private SudokuPuzzle pz9;

        [TestInitialize]
        public void SudokuPuzzleTestInitialize()
        {
            pz9 = new SudokuPuzzle(_easy9x9);
        }

        [TestMethod]
        public void CreateSudokuPuzzle()
        {
            // Check for successful instantiation
            Assert.IsNotNull(pz9);
            Assert.IsInstanceOfType(pz9, typeof(SudokuPuzzle));
            var newPz = new SudokuPuzzle(_easy9x9);
            Assert.AreNotSame(pz9, newPz);
        }

        [TestMethod]
        public void SudokuPuzzleAttributes()
        {
            // Make sure attributes have been created
            Assert.IsNotNull(pz9.Grid);
            Assert.IsInstanceOfType(pz9.Grid, typeof (Square[,]));
            Assert.IsNotNull(pz9.Rows);
            Assert.IsInstanceOfType(pz9.Rows, typeof(List<Square>[]));
            Assert.IsNotNull(pz9.Columns);
            Assert.IsInstanceOfType(pz9.Columns, typeof(List<Square>[]));
            Assert.IsNotNull(pz9.SubGrids);
            Assert.IsInstanceOfType(pz9.SubGrids, typeof(List<Square>[]));

            // Ensure sizes are correct
            Assert.AreEqual(pz9.GridWidth, _width);
            Assert.AreEqual(pz9.MaxValue, _width);
            Assert.AreEqual(pz9.SubGridWidth, _subWidth);
            Assert.AreEqual((uint)pz9.Grid.GetLength(0), _width);
            Assert.AreEqual((uint)pz9.Grid.GetLength(1), _width);
            Assert.AreEqual((uint)pz9.Grid.Cast<Square>().Count(), _squareCount);
            Assert.AreEqual((uint)pz9.Rows.Length, _width);
            Assert.AreEqual((uint)pz9.Columns.Length, _width);
            Assert.AreEqual((uint)pz9.SubGrids.Length, _width);

            // Ensure other attributes are correct
            Assert.IsFalse(pz9.IsSolved);
        }

        [TestMethod]
        public void DeserializeThenReserialize()
        {
            Assert.AreEqual(_easy9x9, SudokuPuzzle.Serialize(pz9));
        }

        [TestMethod]
        public void InvalidSerializedGrids()
        {
            // ReSharper disable UnusedVariable
            // Try using a required blank
            try
            {
                var newPz = new SudokuPuzzle(_invalidRequiredBlank);
                Assert.Fail();
            }
            catch (FormatException)
            { }
            // ReSharper restore UnusedVariable
        }

        [TestMethod]
        public void SolvedPuzzle()
        {
            var ansPz = new SudokuPuzzle(_easy9x9Solution);
            Assert.IsTrue(ansPz.IsSolved);
        }

        [TestMethod]
        public void CheatEasyPuzzle()
        {
            Assert.IsFalse(pz9.IsSolved);

            var ansPz = new SudokuPuzzle(_easy9x9Solution);
            Assert.AreNotSame(pz9, ansPz);

            for (var row = 0; row < _width; row++)
            {
                for (var col = 0; col < _width; col++)
                {
                    if (pz9[row, col].IsBlank)
                    {
                        pz9[row, col].Value = ansPz[row, col].Value;
                    }
                }
            }

            Assert.IsTrue(pz9.IsSolved);
            Assert.AreNotSame(pz9, ansPz);
            Assert.AreEqual(SudokuPuzzle.Serialize(pz9), SudokuPuzzle.Serialize(ansPz));
            Assert.AreEqual(SudokuPuzzle.Serialize(pz9), _easy9x9Solution);
        }

        [TestMethod]
        public void TestClearingNonGivens()
        {
            var ansPz = new SudokuPuzzle(_easy9x9Solution);
            Assert.AreEqual(ansPz.LockedSquaresCount, _numGivens);
            Assert.AreEqual(ansPz.BlankSquaresCount, 0);
            Assert.IsTrue(ansPz.IsSolved);

            ansPz.ClearNonGivens();

            Assert.AreEqual(ansPz.LockedSquaresCount, _numGivens);
            Assert.AreEqual(ansPz.BlankSquaresCount, (int)_squareCount - _numGivens);
            Assert.IsFalse(ansPz.IsSolved);
        }

        [TestMethod]
        public void TestEquivalentArrayAndSerializedString()
        {
            var newPz = new SudokuPuzzle(_easy9x9Array);
            Assert.AreEqual(newPz.LockedSquaresCount, _numGivens);
            Assert.AreEqual(newPz.BlankSquaresCount, (int)_squareCount - _numGivens);
            Assert.IsFalse(newPz.IsSolved);
            Assert.AreEqual(SudokuPuzzle.Serialize(newPz), SudokuPuzzle.Serialize(pz9));
        }
    }
}
