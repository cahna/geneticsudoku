﻿using System;
using System.Data;
using GeneticSudoku.Objects;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GeneticSudokuTests.Objects
{
    [TestClass]
    public class SquareTests
    {
        #region Test Properties
        private const string _easy9x9 = ".,.,6*,.,2*,.,5*,4*,.," +
                                      "4*,2*,.,.,.,9*,.,8*,.," +
                                      "8*,.,7*,.,3*,.,9*,.,.," +
                                      ".,4*,1*,.,.,5*,2*,6*,.," +
                                      "7*,.,.,.,.,.,.,.,1*," +
                                      ".,6*,2*,3*,.,.,8*,7*,.," +
                                      ".,.,5*,.,7*,.,4*,.,8*," +
                                      ".,9*,.,2*,.,.,.,3*,7*," +
                                      ".,7*,8*,.,4*,.,1*,.,.";
        private const uint _maxValue = 9;
        private const uint _emptyValue = 0;
        private const uint _testValue = 4;
        private const bool _testLocked = true;
        #endregion // Test Properties

        private SudokuPuzzle _pz9;
        private Square _sq;
        private Square _newSq;

        [TestInitialize]
        public void SquareTestInitialize()
        {
            _pz9 = new SudokuPuzzle(_easy9x9);
            _sq = new Square(_pz9.MaxValue, _testValue, _testLocked);
            _newSq = null;
        }

        [TestMethod]
        public void CreateSquareExplicitParams()
        {
            Assert.IsNotNull(_sq);
            Assert.IsInstanceOfType(_sq, typeof(Square));
            Assert.AreEqual(_sq.MaxValue, _maxValue);
            Assert.AreEqual(_sq.MaxValue, _pz9.MaxValue);
            Assert.AreEqual(_sq.IsLocked, _testLocked);
            Assert.AreEqual(_sq.Value, _testValue);
            Assert.IsFalse(_sq.IsBlank);
        }

        [TestMethod]
        public void CreateSquareOption1()
        {
            _newSq = new Square(_pz9.MaxValue, _testValue);
            Assert.IsNotNull(_newSq);
            Assert.IsInstanceOfType(_newSq, typeof(Square));
            Assert.AreEqual(_newSq.MaxValue, _maxValue);
            Assert.AreEqual(_newSq.MaxValue, _pz9.MaxValue);
            Assert.IsFalse(_newSq.IsLocked);
            Assert.AreEqual(_newSq.Value, _testValue);
            Assert.IsFalse(_newSq.IsBlank);
        }

        [TestMethod]
        public void CreateInvalidSquare()
        {
            try
            {
                // ReSharper disable UnusedVariable
                _newSq = new Square(_pz9.MaxValue, isLocked: true);
                // ReSharper restore UnusedVariable
                Assert.Fail();
            }
            catch (InvalidConstraintException)
            {
                Assert.IsNull(_newSq);
            }
        }

        [TestMethod]
        public void ClearSquareLockedValue()
        {
            Assert.IsFalse(_sq.IsBlank);
            Assert.IsTrue(_sq.IsLocked);
            try
            {
                _sq.Value = 0;
                Assert.Fail("Able to clear a locked value");
            }
            catch (InvalidOperationException)
            {
                Assert.AreNotEqual(_sq.Value, _emptyValue);
                Assert.IsFalse(_sq.IsBlank);
                Assert.IsTrue(_sq.IsLocked);
            }
        }

        [TestMethod]
        public void ClearSquareValue()
        {
            _newSq = new Square(_pz9.MaxValue, _testValue);
            Assert.IsFalse(_newSq.IsBlank);
            _newSq.Value = 0;
            Assert.IsTrue(_newSq.IsBlank);
        }

        [TestMethod]
        public void SetValueLocked()
        {
            Assert.IsTrue(_sq.IsLocked);

            const uint newVal = 1;
            Assert.IsTrue(newVal <= _sq.MaxValue);
            try
            {
                _sq.Value = newVal;
                Assert.Fail();
            }
            catch (InvalidOperationException)
            { }
        }

        [TestMethod]
        public void SetValue()
        {
            _newSq = new Square(_pz9.MaxValue) { Value = _maxValue };
            Assert.AreEqual(_newSq.Value, _maxValue);
        }

        [TestMethod]
        public void SetValueOutOfBounds()
        {
            _newSq = new Square(_pz9.MaxValue);
            var oob = _sq.MaxValue + 1;
            
            try
            {
                _newSq.Value = oob;
                Assert.Fail();
            }
            catch (InvalidOperationException)
            { }
        }
    }
}
