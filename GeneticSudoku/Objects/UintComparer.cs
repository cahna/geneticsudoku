﻿using System.Collections.Generic;

namespace GeneticSudoku.Objects
{
    class UintComparer : EqualityComparer<uint>
    {
        public override bool Equals(uint x, uint y)
        {
            return x == y;
        }

        public override int GetHashCode(uint x)
        {
            return (int) x;
        }
    }
}