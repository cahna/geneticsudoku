﻿namespace GeneticSudoku.Objects
{
    public enum Selection
    {
        FitnessProportionate,
        Tournament
    }
}