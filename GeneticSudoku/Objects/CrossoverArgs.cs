﻿namespace GeneticSudoku.Objects
{
    public class CrossoverArgs
    {
        public readonly GASudokuPuzzle Sudoku1;
        public readonly GASudokuPuzzle Sudoku2;

        public CrossoverArgs(GASudokuPuzzle p1, GASudokuPuzzle p2)
        {
            Sudoku1 = p1;
            Sudoku2 = p2;
        }
    }
}
