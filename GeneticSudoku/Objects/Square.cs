﻿using System;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.Runtime.CompilerServices;
using GeneticSudoku.Annotations;

namespace GeneticSudoku.Objects
{
    public sealed class Square : INotifyPropertyChanged
    {
        #region Attributes
        /// <summary>
        /// Number in the square
        /// </summary>
        private uint _value;

        private readonly bool _isLocked;

        /// <summary>
        /// Sets max range of square (1, 2, 3, ..., MaxValue)
        /// </summary>
        public readonly uint MaxValue;

        /// <summary>
        /// Is the square a given (locked) position
        /// </summary>
        public bool IsLocked
        {
            get { return _isLocked; }
        }
        
        /// <summary>
        /// Is the square empty (the square does not contain a value)
        /// </summary>
        public bool IsBlank
        {
            get { return _value == 0; }
        }

        /// <summary>
        /// Get or set the value of square (use Clear() to clear square's value)
        /// </summary>
        public uint Value 
        { 
            get { return _value; }
            set
            {
                if(IsLocked) 
                    throw new InvalidOperationException("Not allowed to change value of a given (locked) square");
                if(value > MaxValue)
                    throw new InvalidOperationException("Value is outside of allowed range");

                if (value != _value)
                {
                    _value = value;
                    OnPropertyChanged();
                }
            }
        }
        #endregion // Attributes

        #region Constructor

        /// <summary>
        /// Default constructor: Creates a new square
        /// </summary>
        /// <param name="maxValue">Maximum integer value the square can hold (also the width of the sudoku board</param>
        /// <param name="squareValue">Given value for square</param>
        /// <param name="isLocked">Is the square a given (locked/not editable) value</param>
        public Square(uint maxValue, uint squareValue = 0, bool isLocked = false)
        {
            if(squareValue == 0 && isLocked)
                throw new InvalidConstraintException("Can't create an empty square as locked");
            _isLocked = isLocked;
            MaxValue = maxValue;
            _value = squareValue;
        }
        #endregion // Constructor

        #region Public Methods

        /// <summary>
        /// Get a string representation of the square's value
        /// </summary>
        /// <returns>Square's value</returns>
        public override string ToString()
        {
            return IsBlank ? "." : _value.ToString(CultureInfo.InvariantCulture);
        }

        public Square ShallowCopy()
        {
            return (Square) MemberwiseClone();
        }

        public Square DeepCopy()
        {
            var s = ShallowCopy();
            // Fill in any objects' deep/shallow copies here
            return s;
        }

        #endregion // Public Methods

        #region IPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) 
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion // IPropertyChanged
    }
}