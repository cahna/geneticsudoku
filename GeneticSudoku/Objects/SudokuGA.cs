﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using GeneticSudoku.Annotations;

namespace GeneticSudoku.Objects
{
    public class SudokuGA : INotifyPropertyChanged
    {
        public static bool Verbose = false;
        public static void Debug(string msg)
        {
            if(Verbose)
                Console.WriteLine(msg);
        }

        #region Constants
        private const string _serializedErr = "Must provide a non-null, non-empty serialized puzzle string";
        private const string _tourneySizeErr = "Tournament selection must use a tournament size > 0";
        #endregion // Constants

        #region Attributes
        public readonly double MutationRate;
        public readonly uint PopulationSize;
        public readonly string SerializedPuzzle;
        public readonly uint MaxGenerations;
        public readonly Selection CrossoverSelection;
        public readonly uint TournamentSize;
        public uint CurrentGeneration { get; private set; }
        public readonly Random GARandom;
        public readonly Stopwatch W;
        private readonly List<GASudokuPuzzle> _population;

        public List<GASudokuPuzzle> Population
        {
            get { return _population; }
        }

        [UsedImplicitly] private GASudokuPuzzle _mostFit;
        public GASudokuPuzzle MostFit
        {
            get {
                return _mostFit;
            }
            private set 
            { 
                _mostFit = value;
                OnPropertyChanged();
            }
        }
        public int UniqueFitnesses
        {
            get
            {
                return Population.Select(p => p.Fitness).Distinct().OrderBy(v => v).Count();
            }
        }

        private readonly int _winSize;
        private readonly Queue<int> _window;

        #endregion // Attributes

        #region Constructors

        public SudokuGA(string serializedPuzzle, double mutationRate, uint populationSize,
                        uint generations = 0, Selection crossoverSelection = Selection.Tournament,
                        uint tourneySize = 0, int? randomseed = null)
        {
            if(string.IsNullOrEmpty(serializedPuzzle))
                throw new ArgumentException(_serializedErr);
            if(crossoverSelection == Selection.Tournament && tourneySize < 1)
                throw new ArgumentException(_tourneySizeErr);

            MutationRate = mutationRate;
            PopulationSize = populationSize;
            SerializedPuzzle = serializedPuzzle;
            MaxGenerations = generations;
            CrossoverSelection = crossoverSelection;
            TournamentSize = tourneySize;
            CurrentGeneration = 0;
            GARandom = randomseed != null ? new Random((int) randomseed) : new Random();
            _winSize = 1000;
            _window = new Queue<int>(_winSize);
            _population = new List<GASudokuPuzzle>((int)PopulationSize);
            W = new Stopwatch();
        }

        #endregion // Constructors

        #region Private Methods
        private static void ForkActions(IList<Action> a)
        {
            var actions = new Task[a.Count];
            for (var i = 0; i < a.Count; i++)
                actions[i] = Task.Factory.StartNew(a[i]);
            Task.WaitAll(actions);
        }

        private CrossoverArgs TournamentSelection(uint tournamentSize)
        {
            var p1Index = -1;
            var p2Index = -1;

            // Select the first parent
            var popIndices = Enumerable.Range(0, _population.Count).ToList();
            for (var k = 0; k < tournamentSize; k++)
            {
                var popIndex = popIndices[GARandom.Next(popIndices.Count())];
                popIndices.Remove(popIndex);
                if (p1Index < 0 || _population[popIndex].Fitness < _population[p1Index].Fitness)
                    p1Index = popIndex;
            }

            // Select the second parent
            popIndices = Enumerable.Range(0, _population.Count).ToList();
            popIndices.Remove(p1Index); // Don't select the same parent twice
            for (var k = 0; k < tournamentSize; k++)
            {
                var popIndex = popIndices[GARandom.Next(popIndices.Count())];
                popIndices.Remove(popIndex);
                if (p2Index < 0 || _population[popIndex].Fitness < _population[p2Index].Fitness)
                    p2Index = popIndex;
            }

            if (p1Index < 0 || p2Index < 0)
                throw new NullReferenceException("No crossover points were selected");

            Debug(string.Format("Selected for parentage: {0} {1}", p1Index, p2Index));
            return new CrossoverArgs(_population[p1Index], _population[p2Index]);
        }

        private CrossoverArgs FitnessProportionateSelection()
        {
            throw new NotImplementedException();
        }

        private CrossoverArgs SelectCrossover()
        {
            CrossoverArgs selected = null;
            switch (CrossoverSelection)
            {
                case Selection.Tournament:
                    selected = TournamentSelection(TournamentSize);
                    break;
                case Selection.FitnessProportionate:
                    break;
                default:
                    throw new InvalidEnumArgumentException("Invalid crossover selection method");
            }

            if (selected == null)
                throw new InvalidOperationException("Failed to select parents");

            return selected;
        }

        private void Combine(IReadOnlyList<Square> p1group, IReadOnlyList<Square> p2group, IReadOnlyList<Square> c1group, IReadOnlyList<Square> c2group, bool randomly = false)
        {
            if(c1group == null || c2group == null || p1group == null || p2group == null)
                throw new InvalidOperationException("Child groups must be initialized before combining");
            if(p1group.Count != p2group.Count || c1group.Count != c2group.Count || p1group.Count != c1group.Count)
                throw new InvalidOperationException("Parent and child groupings must be of the same length");

            var subLengh = p1group.Count;

            if (randomly)
            {
                var p1Indices = new List<int>();
                var p2Indices = new List<int>();
                for (var i = 0; i < p1Indices.Count; i++)
                    if(!p1group[i].IsLocked)
                        p1Indices.Add(i);
                p2Indices.AddRange(p1Indices);

                while (p1Indices.Count > 0)
                {
                    var i1 = p1Indices[GARandom.Next(p1Indices.Count)];
                    var i2 = p2Indices[GARandom.Next(p2Indices.Count)];

                    c1group[i1].Value = p2group[i2].Value;
                    c2group[i2].Value = p1group[i1].Value;

                    p1Indices.Remove(i1);
                    p2Indices.Remove(i2);
                }
            }
            else
            {
                for (var position = 0; position < subLengh; position++)
                {
                    if (p1group[position].IsLocked) continue;
                    c1group[position].Value = p2group[position].Value;
                    c2group[position].Value = p1group[position].Value;
                }
            }
        }

        private CrossoverArgs Crossover(CrossoverArgs parents)
        {
            var parent1 = parents.Sudoku1;
            var parent2 = parents.Sudoku2;
            var subLengh = parent1.Rows.Count();

            // Make copies of the parents so as to be non-destructive to the board object references
            // child1 will crossover by row. child2 will crossover by column
            var child1 = new GASudokuPuzzle(parent1);
            var child2 = new GASudokuPuzzle(parent2);

            // Select crossover point
            var crossPoint = GARandom.Next(subLengh);

            //var useRow = GARandom.Next(2) == 1;
            var p1Group = parent1.SubGrids;//useRow ? parent1.Rows : parent1.Columns;
            var p2Group = parent2.SubGrids;//useRow ? parent2.Rows : parent2.Columns;
            var c1Group = child1.SubGrids;//useRow ? child1.Rows : child1.Columns;
            var c2Group = child2.SubGrids;//useRow ? child1.Rows : child1.Columns;
            
            // Swap the rows after the crossover point
            for (var cpoint = crossPoint; cpoint < subLengh; cpoint++)
                Combine(p1Group[cpoint], p2Group[cpoint], c1Group[cpoint], c2Group[cpoint], GARandom.NextDouble() < 0.05);

            //ReplaceAllRepeatsInPuzzle(child1);
            //ReplaceAllRepeatsInPuzzle(child2);

            return new CrossoverArgs(child1, child2);
        }

        private void ReplaceRepeatsFromSubGroup(IEnumerable<List<Square>> subGroup, List<uint> possiblevalues)
        {
            subGroup = subGroup.ToArray();
            foreach (var g in subGroup)
            {
                var valCounts = possiblevalues.ToDictionary(v => v, v => 0);

                foreach (var sq in g)
                    valCounts[sq.Value]++;

                if (!valCounts.Values.Any(v => v > 1)) continue;

                var dupVals = valCounts.Where(kv => kv.Value > 1).ToList();
                var possibleVals = possiblevalues.Except(dupVals.Select(s => s.Key).ToList()).ToList();
                var g1 = g;
                foreach (var dupSquares in dupVals.Select(kvp => g1.Where(s => s.Value == kvp.Value && !s.IsLocked).ToList()))
                {
                    while (dupSquares.Count > 1)
                    {
                        var sq = dupSquares.First();
                        var val = possibleVals[GARandom.Next(possibleVals.Count)];
                        sq.Value = val;
                        dupSquares.Remove(sq);
                        possibleVals.Remove(val);
                    }
                }
            }
        }

        private void ReplaceAllRepeatsInPuzzle(SudokuPuzzle p)
        {
            //ReplaceRepeatsFromSubGroup(p.Rows, p.PossibleValues);
            ReplaceRepeatsFromSubGroup(p.Columns, p.PossibleValues);
            ReplaceRepeatsFromSubGroup(p.Rows, p.PossibleValues);
            
            ReplaceRepeatsFromSubGroup(p.SubGrids, p.PossibleValues);
        }

        private void ReplaceAllRepeats()
        {
            var actions = new Task[_population.Count];
            for (var i = 0; i < _population.Count; i++)
            {
                var puzzle = _population[i];
                actions[i] = Task.Factory.StartNew(() => ReplaceAllRepeatsInPuzzle(puzzle));
            }
            Task.WaitAll(actions);
        }

        #endregion // Private Methods

        #region Public Methods
        
        /// <summary>
        /// Generates the evolutionary algorithm-based system's initial _population
        /// </summary>
        /// <param name="inParallel">Explicitly enable or disable filling the boards in parallel (optional, default: true)</param>
        public void GeneratePopulation(bool inParallel = true)
        {
            for (var i = 0; i < PopulationSize; i++)
                _population.Add(new GASudokuPuzzle(this, SerializedPuzzle));

            ClearPopulationAndRefillRandomly();
        }

        public void ClearPopulationAndRefillRandomly()
        {
            var clearActions = new Action[PopulationSize];
            for (var i = 0; i < PopulationSize; i++)
                clearActions[i] = _population[i].ClearNonGivens;
            ForkActions(clearActions);

            var fillBlanksActions = new Action[PopulationSize];
            for (var i = 0; i < PopulationSize; i++)
                fillBlanksActions[i] = _population[i].FillBlanksSmartRandomly;
            ForkActions(fillBlanksActions);

            /*foreach (var p in _population)
            {
                p.ClearNonGivens();
                p.FillBlanksSmartRandomly();
            }
             * */

            ReplaceAllRepeats();
            MostFit = _population.OrderBy(p => p.Fitness).First();
        }

        public void Run()
        {
            //while(CurrentGeneration < MaxGenerations && !Population.Any(p => p.IsSolved))
            while(!Population.Any(p => p.IsSolved))
                NextGeneration();
        }

        public void NextGeneration()
        {
            // Disallow any generation past the MaxGeneration (if MaxGenerations is 0, run infinitely)
            if (MaxGenerations != 0 && CurrentGeneration >= MaxGenerations) return;

            W.Start();

            // If the population has converged to a local minimum, nuke the population from orbit
            /*if (_window.Count >= _winSize && _window.Distinct().Count() < 2)
            {
                _window.Clear();
                ClearPopulationAndRefillRandomly();
            }*/

            // Perform mating and mutation
            for (var crosses = 0; crosses < 2; crosses++)
            {
                // Select the sudoku boards to be used for crossover
                var selectedParents = SelectCrossover();

                // Perform crossover
                var cArgs = Crossover(selectedParents);
                var c1 = cArgs.Sudoku1;
                var c2 = cArgs.Sudoku2;
                var children = new List<GASudokuPuzzle> {c1, c2};

                // Perform mutation if its time
                foreach (var child in children.Where(child => GARandom.NextDouble() < MutationRate))
                {
                    GASudokuPuzzle.Mutate(child, GARandom);
                    if(GARandom.NextDouble() < MutationRate)
                        child.MutateWithinSubGrid();
                    if (GARandom.NextDouble() < MutationRate)
                        ReplaceAllRepeatsInPuzzle(child);
                }

                // Replace less-fit predecessors
                foreach (var child in children)
                {
                    if (!_population.Any(p => p.Fitness > child.Fitness)) continue;

                    // Children replace less fit predecessors
                    var lessFitThanChild = _population.Where(p => p.Fitness > child.Fitness).ToList();
                    if (!lessFitThanChild.Any()) continue;

                    lessFitThanChild = lessFitThanChild.OrderByDescending(p => p.Fitness).ToList();
                    var toReplace = lessFitThanChild.First().Grid.Cast<Square>().ToList();
                    var childList = child.Grid.Cast<Square>().ToList();
                    for (var i = 0; i < toReplace.Count; i++)
                    {
                        if (toReplace[i].IsLocked) continue;
                        toReplace[i].Value = childList[i].Value;
                    }
                }
            }

            // Log best fitness
            while (_window.Count >= _winSize)
                _window.Dequeue();

            MostFit = _population.OrderBy(p => p.Fitness).First();
            _window.Enqueue(MostFit.Fitness);

            // Increment generation counter
            CurrentGeneration++;

            W.Stop();
        }

        #endregion // Public Methods

        #region IPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion // IPropertyChanged
    }
}
