﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GeneticSudoku.Objects;

namespace SudokuGUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const string p16x16 = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16";
        private const string _easy9x9 = ".,.,6*,.,2*,.,5*,4*,.," +
                                        "4*,2*,.,.,.,9*,.,8*,.," +
                                        "8*,.,7*,.,3*,.,9*,.,.," +
                                        ".,4*,1*,.,.,5*,2*,6*,.," +
                                        "7*,.,.,.,.,.,.,.,1*," +
                                        ".,6*,2*,3*,.,.,8*,7*,.," +
                                        ".,.,5*,.,7*,.,4*,.,8*," +
                                        ".,9*,.,2*,.,.,.,3*,7*," +
                                        ".,7*,8*,.,4*,.,1*,.,.";
        private const string medium9x9 = ".,.,1*,.,7*,.,.,6*,8*,4*,5*,.,.,.,1*,9*,3*,.,6*,.,.,.,.,3*,1*,.,.,5*,1*,.,.,.,.,.,.,.,.,.,6*,.,.,.,4*,.,.,.,.,.,.,.,.,.,7*,3*,.,.,5*,3*,.,.,.,.,4*,.,2*,7*,6*,.,.,.,1*,9*,9*,3*,.,.,1*,.,8*,.,.";
        private const string med2_9x9 = "2*,.,.,.,.,5*,8*,.,1*,.,.,7*,.,4*,.,.,2*,.,.,.,5*,2*,.,.,3*,6*,.,6*,.,.,.,3*,.,.,.,2*,.,.,.,4*,1*,7*,.,.,.,1*,.,.,.,5*,.,.,.,3*,.,8*,2*,.,.,3*,5*,.,.,.,9*,.,.,2*,.,7*,.,.,5*,.,6*,1*,.,.,.,.,9*";
        private const string hard_9x9 = ".,.,.,.,3*,.,.,6*,.,5*,.,.,.,4*,6*,8*,2*,7*,.,.,8*,.,.,.,.,.,.,.,.,.,5*,.,8*,.,.,6*,.,3*,.,.,9*,.,.,5*,.,4*,.,.,3*,.,1*,.,.,.,.,.,.,.,.,.,3*,.,.,3*,1*,6*,9*,8*,.,.,.,4*,.,2*,.,.,1*,.,.,.,.";
        private const string med2mask = "111101101011100000110111111111010110101101101011010111111111011000001110101101111";
        private const string p9x9_blank = ".,.,.,.,.,.,.,.,.," +
                                          ".,.,.,.,.,.,.,.,.," +
                                          ".,.,.,.,.,.,.,.,.," +
                                          ".,.,.,.,.,.,.,.,.," +
                                          ".,.,.,.,.,.,.,.,.," +
                                          ".,.,.,.,.,.,.,.,.," +
                                          ".,.,.,.,.,.,.,.,.," +
                                          ".,.,.,.,.,.,.,.,.," +
                                          ".,.,.,.,.,.,.,.,.";

        private const string p4x4_blank = ".,.,.,.," +
                                          ".,.,.,.," +
                                          ".,.,.,.," +
                                          ".,.,.,.";

        private const double MRate = 0.85;
        private const uint PopSize = 1200;
        private const uint Gens = 0;
        private const uint TSize = 3;
        private const int seed = 77;

        private readonly SudokuGA ga;

        private BackgroundWorker _worker;
        private bool _updateMostFit = true;

        public MainWindow()
        {
            ga = new SudokuGA(hard_9x9, MRate, PopSize, Gens, tourneySize: TSize);
            ga.GeneratePopulation();

            InitializeComponent();

            //lst.ItemsSource = ga.MostFit.Rows;
            SudokuGridHolder.Children.Add(new ContentControl {Content = ga});

            //InitializeComponent();
        }

        private void _workerRunSimDoWork(object sender, DoWorkEventArgs e)
        {
            var worker = sender as BackgroundWorker;
            while (worker != null && (!ga.Population.Any(p => p.IsSolved) && !worker.CancellationPending))
            {
                ga.NextGeneration();
                worker.ReportProgress(0, new[] {ga.CurrentGeneration, (uint) ga.MostFit.Fitness});
            }
        }

        private void _workerRunSimCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if(ga.Population.Any(p => p.IsSolved))
                RunButton.Background = new SolidColorBrush(Colors.Yellow);
            RunButton.IsEnabled = true;
        }

        private void _workerRunSimProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            var args = e.UserState as uint[];
            if (args != null)
            {
                var gen = args[0];
                var fit = args[1];
                GenerationsLabel.Content = gen;
                FitLabel.Content = fit;
                UniqueCountLabel.Content = ga.UniqueFitnesses;
                //CompleteLabel.Content = ga.IsSolved.ToString();
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            RunButton.IsEnabled = false;
            _worker = new BackgroundWorker
                {
                    WorkerReportsProgress = true,
                    WorkerSupportsCancellation = true
                };
            _worker.DoWork += _workerRunSimDoWork;
            _worker.RunWorkerCompleted += _workerRunSimCompleted;
            _worker.ProgressChanged += _workerRunSimProgressChanged;
            

            _worker.RunWorkerAsync();
        }

        private void StopButton_Click(object sender, RoutedEventArgs e)
        {
            _worker.CancelAsync();
        }
    }
}
